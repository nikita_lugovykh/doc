const header_burger = document.querySelector('.header__burger');
const menu = document.querySelector('.menu');
const body_lock = document.querySelector('body');

header_burger.onclick = function() {
    header_burger.classList.toggle('active');
    menu.classList.toggle('active');
    body_lock.classList.toggle('lock');

}

// функция прокрутки вниз с header

function fn() {

    const btnScrollDown = document.querySelector('#header__btn-scroll-down');
    
    function scrollDown() {
        
        // переменная с записанной в нее данных о высоте видимости окна - выглядит так: 
        const windowCoords = document.documentElement.clientHeight;
        

        (function scroll() {
            if (window.pageYOffset < windowCoords) {
                window.scrollBy(0, 10);
                setTimeout(scroll, 0);
            }
            if (window.pageYOffset > windowCoords) {
                window.scrollTo(0, windowCoords);
            }
        })();
    }
    btnScrollDown.addEventListener('click', scrollDown);
}
Вызов функции выглядит так:
fn()
